package me.rparra.pm_pixtr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.utils.GeneralUtils;
import me.rparra.pm_pixtr.utils.PreferencesManager;

public class SplashActivity extends AppCompatActivity {
    private final String TAG = SplashActivity.class.getName();

    // Constants
    private final int SPLASH_DELAY = 2500;

    // Views
    protected @BindView(R.id.activity_splash_layout_flag) LinearLayout layoutFlag;
    protected @BindView(R.id.activity_splash_text_view_slogan) TextView textSlogan;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    protected void setupViews() {
        Log.d(TAG, "setupViews: setting up views...");

        // Setup views with ButterKnife
        ButterKnife.bind(this);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) (getResources().getDisplayMetrics().density * 3)
        );
        layoutParams.setMargins(0, GeneralUtils.getStatusBarHeight(this), 0, 0);
        layoutFlag.setLayoutParams(layoutParams);
        textSlogan.setText(String.format("\"%s\"", GeneralUtils.getRandomSlogan()));
    }

    private void waitForSplash() {
        Log.d(TAG, "waitForSplash: waiting...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "waitForSplash: finish!");
                if (PreferencesManager.getTutorialStatus(getApplicationContext())) {
                    Log.d(TAG, "waitForSplash: go to camera app");
                    Intent intent = new Intent(SplashActivity.this, CameraActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Log.d(TAG, "waitForSplash: go to tutorial");
                    Intent intent = new Intent(SplashActivity.this, TutorialActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }, SPLASH_DELAY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setupViews();
        waitForSplash();
    }
}
