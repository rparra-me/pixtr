package me.rparra.pm_pixtr.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import net.bohush.geometricprogressview.GeometricProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.utils.StorageManager;
import me.rparra.pm_pixtr.utils.GeneralUtils;
import me.rparra.pm_pixtr.utils.PreferencesManager;

public class CameraActivity extends AppCompatActivity {
    private final String TAG = CameraActivity.class.getName();

    // Views
    protected @BindView(R.id.activity_camera_toolbar) Toolbar toolbar;
    protected @BindView(R.id.activity_camera_camera_view) CameraView cameraView;
    protected @BindView(R.id.activity_camera_layout_overlay) RelativeLayout layoutOverlay;
    protected @BindView(R.id.activity_camera_layout_container_toolbar) RelativeLayout layoutContainerToolbar;
    protected @BindView(R.id.activity_camera_layout_container_shutter) LinearLayout layoutContainerShutter;
    protected @BindView(R.id.activity_camera_image_button_shutter) ImageButton buttonShutter;
    protected @BindView(R.id.activity_camera_image_button_switch_camera) ImageButton buttonSwitchCamera;
    protected @BindView(R.id.activity_camera_circle_image_view_last_shot) CircleImageView imageLastShot;
    protected @BindView(R.id.activity_camera_geometric_progress_view) GeometricProgressView progressView;

    // Etc.
    private Menu menu;

    // Listeners
    private CameraKitEventListener cameraListener = new CameraKitEventListener() {
        @Override
        public void onEvent(CameraKitEvent cameraKitEvent) {

        }

        @Override
        public void onError(CameraKitError cameraKitError) {

        }

        @Override
        public void onImage(CameraKitImage cameraKitImage) {
            AsyncSavePhoto savePhoto = new AsyncSavePhoto();
            savePhoto.execute(cameraKitImage.getJpeg());}

        @Override
        public void onVideo(CameraKitVideo cameraKitVideo) {

        }
    };

    /**
     * OnClick listener for shutter button
     * @param view
     */
    @OnClick(R.id.activity_camera_image_button_shutter)
    public void onClickButtonShutter(View view) {
        Log.d(TAG, "onClickButtonShutter: click");
        if (cameraView != null) {
            cameraView.captureImage();
            Log.d(TAG, "onClick: image captured!");
        }
    }

    /**
     * OnClick listener for switch camera button
     * @param view
     */
    @OnClick(R.id.activity_camera_image_button_switch_camera)
    public void onClickButtonSwitchCamera(View view) {
        Log.d(TAG, "onClickButtonSwitchCamera: click");
        boolean currentStatus = PreferencesManager.getSelfieCameraStatus(CameraActivity.this);
        PreferencesManager.setSelfieCamera(CameraActivity.this, !currentStatus);
        switchCamera();
    }

    /**
     * OnClick listener for last shot image view
     * @param view
     */
    @OnClick(R.id.activity_camera_circle_image_view_last_shot)
    public void onClickImageLastShot(View view) {
        Intent imageViewer = new Intent(CameraActivity.this, PhotoViewerActivity.class);
        startActivity(imageViewer);
    }

    private void setupViews() {
        Log.d(TAG, "setupViews: setting up views...");

        // Bind the views with ButterKnife
        ButterKnife.bind(this);

        // Create a fullscreen activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }

        // Setup Toolbar
        RelativeLayout.LayoutParams layoutParamsToolbar = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParamsToolbar.setMargins(0, GeneralUtils.getStatusBarHeight(this), 0, 0);
        layoutContainerToolbar.setLayoutParams(layoutParamsToolbar);

        toolbar.inflateMenu(R.menu.camera_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_camera_flash) {
                    boolean flashStatus = PreferencesManager.getFlashStatus(CameraActivity.this);
                    PreferencesManager.setFlash(CameraActivity.this, !flashStatus);
                    switchFlash();
                }
                return false;
            }
        });
        menu = toolbar.getMenu();

        // Setup camera
        cameraView.addCameraKitListener(cameraListener);

        // Setup Shutter layout
        RelativeLayout.LayoutParams layoutParamsButton = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) (getResources().getDisplayMetrics().density * 140) // 120dp
        );
        layoutParamsButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParamsButton.setMargins(0, 0, 0, GeneralUtils.getNavigationBarHeight(this));
        layoutContainerShutter.setLayoutParams(layoutParamsButton);

        // Setup loading overlay
        layoutOverlay.setVisibility(View.GONE);
        progressView.setColor(GeneralUtils.getRandomAccentColor());
        progressView.setVisibility(View.GONE);
    }


    /**
     * Switches the camera
     */
    private void switchCamera() {
        if (PreferencesManager.getSelfieCameraStatus(this)) {
            cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
        } else {
            cameraView.setFacing(CameraKit.Constants.FACING_BACK);
        }
    }

    /**
     * Switches flash feature
     */
    private void switchFlash() {
        if (PreferencesManager.getFlashStatus(this)) {
            menu.getItem(0).setIcon(R.drawable.ic_flash);
            cameraView.setFlash(CameraKit.Constants.FLASH_ON);
        } else {
            menu.getItem(0).setIcon(R.drawable.ic_flash_off);
            cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
        }
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        AsyncLoadLastShot loadLastPicture = new AsyncLoadLastShot();
        loadLastPicture.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        setupViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switchCamera();
        switchFlash();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    /**
     * Asynchronously saves a photo
     */
    private class AsyncSavePhoto extends AsyncTask<byte[], byte[], byte[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.setColor(GeneralUtils.getRandomAccentColor());
            layoutOverlay.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected byte[] doInBackground(byte[]... params) {
            String timestamp = GeneralUtils.getUnixTimeStamp();
            StorageManager.savePicture(getApplicationContext(), params[0], timestamp);
            return new byte[0];
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutOverlay.setVisibility(View.GONE);
                    progressView.setVisibility(View.GONE);
                }
            }, 800); // USER EXPERIENCE! (?)
            AsyncLoadLastShot loadLastPicture = new AsyncLoadLastShot();
            loadLastPicture.execute();
        }
    }

    /**
     * Asynchronously loads the last shot
     */
    private class AsyncLoadLastShot extends AsyncTask<String, String, String> {
        private Bitmap preview;

        @Override
        protected String doInBackground(String... params) {
            preview = StorageManager.loadLastPicture(getApplicationContext());

            if (preview != null) {
                preview = GeneralUtils.resizeBitmap(preview, 640, 480);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (preview != null) {
                imageLastShot.setEnabled(true);
                imageLastShot.setImageBitmap(preview);
            } else {
                imageLastShot.setEnabled(false);
            }
        }
    }
}
