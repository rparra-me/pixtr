package me.rparra.pm_pixtr.activities;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import net.bohush.geometricprogressview.GeometricProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.adapters.FiltersAdapter;
import me.rparra.pm_pixtr.utils.StorageManager;
import me.rparra.pm_pixtr.utils.GeneralUtils;
import me.rparra.pm_pixtr.utils.OpenCvWrapper;

public class PhotoEditorActivity extends AppCompatActivity {
    private final String TAG = PhotoEditorActivity.class.getName();

    // Extras
    public static final String EXTRA_FILENAME = "me.rparra.pm_pixtr.EXTRA_FILENAME";
    private String filename;

    // Views
    protected @BindView(R.id.activity_photo_editor_layout_loading_overlay) RelativeLayout layoutLoadingOverlay;
    protected @BindView(R.id.activity_photo_editor_layout_buttons_container) LinearLayout layoutButtonsContainer;
    protected @BindView(R.id.activity_photo_editor_layout_recycler_view_filters_container) LinearLayout layoutRecyclerViewContainer;
    protected @BindView(R.id.activity_photo_editor_recycler_view_filters) RecyclerView recyclerViewFilters;
    protected @BindView(R.id.activity_photo_editor_image_view_photo) SubsamplingScaleImageView imageViewPhoto;
    protected @BindView(R.id.activity_photo_editor_geometric_progress_view) GeometricProgressView progressView;
    protected @BindView(R.id.activity_photo_editor_button_ok) Button buttonOk;
    protected @BindView(R.id.activity_photo_editor_button_cancel) Button buttonCancel;

    // Etc.
    private Bitmap originalPhoto;
    private int filterNumber = 0;

    /**
     * OnClick listener for the OK button
     * @param view
     */
    @OnClick(R.id.activity_photo_editor_button_ok)
    public void onClickButtonOk(View view) {
        AsyncSavePhoto savePhoto = new AsyncSavePhoto();
        savePhoto.execute();
    }

    /**
     * OnClick listener for the cancel button
     * @param view
     */
    @OnClick(R.id.activity_photo_editor_button_cancel)
    public void onClickButtonCancel(View view) {
        finish();
    }

    private void setupViews() {
        Log.d(TAG, "setupViews: setting up views...");

        // Bind views using ButterKnife
        ButterKnife.bind(this);

        // Top bar setup
        RelativeLayout.LayoutParams topParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) (getResources().getDisplayMetrics().density * 64) // 64dp
        );
        topParams.setMargins(0, GeneralUtils.getStatusBarHeight(this), 0, 0);
        layoutButtonsContainer.setLayoutParams(topParams);

        // Bottom bar setup
        RelativeLayout.LayoutParams bottomParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) (getResources().getDisplayMetrics().density * 100) // 100dp
        );
        bottomParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        bottomParams.setMargins(0, 0, 0, GeneralUtils.getNavigationBarHeight(this));
        layoutRecyclerViewContainer.setLayoutParams(bottomParams);

        // Filters recycler view setup
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewFilters.setLayoutManager(layoutManager);
        recyclerViewFilters.setItemAnimator(new DefaultItemAnimator());

        // Progress view setup
        layoutLoadingOverlay.setVisibility(View.GONE);
        progressView.setColor(GeneralUtils.getRandomAccentColor());
        progressView.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_editor);
        setupViews();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.filename = extras.getString(EXTRA_FILENAME);
            AsyncLoadEditablePhoto loadEditablePhoto = new AsyncLoadEditablePhoto();
            loadEditablePhoto.execute(filename);
        } else {
            finish();
        }
    }

    /**
     * Finishes the activity
     * HACK: this is how finish() can be accessed from an async task
     */
    private void done() {
        finish();
    }

    /**
     * Asynchronously loads the photo
     */
    private class AsyncLoadEditablePhoto extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            if (params.length != 0) {
                originalPhoto = StorageManager.loadPicture(getApplicationContext(), params[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (originalPhoto != null && imageViewPhoto != null) {
                imageViewPhoto.setImage(ImageSource.bitmap(originalPhoto));
                AsyncLoadAvailableFilters loadAvailableFilters = new AsyncLoadAvailableFilters();
                loadAvailableFilters.execute(originalPhoto);
            }
        }
    }

    /**
     * Asynchronously loads the available filters
     */
    private class AsyncLoadAvailableFilters extends AsyncTask<Bitmap, String, String> {
        private FiltersAdapter filtersAdapter;

        @Override
        protected void onPreExecute() {
            progressView.setColor(GeneralUtils.getRandomAccentColor());
            layoutLoadingOverlay.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            if (params.length != 0) {
                filtersAdapter = new FiltersAdapter(params[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            recyclerViewFilters.setAdapter(filtersAdapter);
            filtersAdapter.notifyDataSetChanged();
            filtersAdapter.setOnItemClickListener(new FiltersAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int filterPosition) {
                    Log.d(TAG, "onItemClick: filter position: " + filterPosition);
                    filterNumber = filterPosition;
                    AsyncFilterPhoto filterPhoto = new AsyncFilterPhoto();
                    filterPhoto.execute(filterPosition);
                }
            });

            layoutLoadingOverlay.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
        }
    }


    /**
     * Asynchronously filters a photo
     */
    private class AsyncFilterPhoto extends AsyncTask<Integer, Integer, Integer> {
        private Bitmap filteredPhoto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.setColor(GeneralUtils.getRandomAccentColor());
            layoutLoadingOverlay.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            if (params.length != 0) {
                Bitmap unfilteredPhoto = StorageManager.loadPicture(getApplicationContext(), filename); // FIXME :(
                filteredPhoto = new OpenCvWrapper().filterFromPosition(params[0], unfilteredPhoto);
            }
            return null;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            imageViewPhoto.setImage(ImageSource.bitmap(filteredPhoto));
            layoutLoadingOverlay.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
        }
    }

    /**
     * Asynchronously saves the photo
     */
    private class AsyncSavePhoto extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.setColor(GeneralUtils.getRandomAccentColor());
            layoutLoadingOverlay.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            // FIXME: :(
            Bitmap unfiltered = StorageManager.loadPicture(getApplicationContext(), filename);
            Bitmap filtered = new OpenCvWrapper().filterFromPosition(filterNumber, unfiltered);
            StorageManager.savePicture(getApplicationContext(), filtered, GeneralUtils.getUnixTimeStamp());
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            layoutLoadingOverlay.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
            done();
        }
    }
}
