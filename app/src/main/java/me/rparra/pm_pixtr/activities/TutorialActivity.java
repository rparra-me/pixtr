package me.rparra.pm_pixtr.activities;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.fragments.TutorialFragment;
import me.rparra.pm_pixtr.utils.GeneralUtils;
import me.rparra.pm_pixtr.utils.PreferencesManager;

public class TutorialActivity extends FragmentActivity {
    private final String TAG = TutorialActivity.class.getName();

    // Views
    protected @BindView(R.id.activity_tutorial_layout_container_indicator) LinearLayout containerIndicator;
    protected @BindView(R.id.activity_tutorial_layout_container_button) LinearLayout containerButton;
    protected @BindView(R.id.activity_tutorial_circle_indicator) CircleIndicator circleIndicator;
    protected @BindView(R.id.activity_tutorial_view_pager) ViewPager viewPager;
    protected @BindView(R.id.activity_tutorial_button_finish) Button buttonFinish;

    // Resources
    protected @BindColor(R.color.colorAccentRed) int colorAccentRed;
    protected @BindColor(R.color.colorAccentYellow) int colorAccentYellow;
    protected @BindColor(R.color.colorAccentGreen) int colorAccentGreen;
    protected @BindString(R.string.permissions_needed) String permissionsNeeded;

    // Etc.
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    /**
     * OnClick Listener for finish button
     * @param view
     */
    @OnClick(R.id.activity_tutorial_button_finish)
    public void buttonFinishOnClick(View view) {
        Log.d(TAG, "buttonFinishOnClick: click");
        handlePermissionsRequest();
    }

    /**
     * Setup views
     */
    private void setupViews() {
        Log.d(TAG, "setupViews: setting up views...");

        // Bind the views with ButterKnife
        ButterKnife.bind(this);

        // Setup the container layouts
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(0, GeneralUtils.getStatusBarHeight(this), 0, 0);
        containerIndicator.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParamsButton = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParamsButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParamsButton.setMargins(0, 0, 0, GeneralUtils.getNavigationBarHeight(this));
        containerButton.setLayoutParams(layoutParamsButton);

        // Setup the viewpager
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionsPagerAdapter);
        circleIndicator.setViewPager(viewPager);
        viewPager.setOnPageChangeListener(new CustomOnPageChangeListener());
    }

    /**
     * Uses Dexter to handle the permissions request
     */
    private void handlePermissionsRequest() {
        Dexter.withActivity(TutorialActivity.this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Log.d(TAG, "onPermissionsChecked: all permissions granted!");
                    finishTutorial();
                } else {
                    Log.e(TAG, "onPermissionsChecked: user rejected at least one permission");
                    Toast.makeText(TutorialActivity.this, permissionsNeeded, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    /**
     * Finishes this tutorial and goes to Camera Activity.
     */
    protected void finishTutorial() {
        Log.d(TAG, "finishTutorial: tutorial finished!");
        PreferencesManager.setTutorialStatus(TutorialActivity.this, true);
        Intent intent = new Intent(TutorialActivity.this, CameraActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        setupViews();
    }

    /**
     * Custom Fragment Pager Adapter
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TutorialFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }
    }

    /**
     * Custom On Page Change Listener
     */
    private class CustomOnPageChangeListener implements ViewPager.OnPageChangeListener {
        private Integer[] colors;

        public CustomOnPageChangeListener() {
            this.colors = new Integer[]{colorAccentRed, colorAccentYellow, colorAccentGreen};
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (position < (sectionsPagerAdapter.getCount() - 1) && (position < (colors.length - 1))) {
                viewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset, colors[position], colors[position + 1]));
            } else {
                viewPager.setBackgroundColor(colors[colors.length - 1]);
            }
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
