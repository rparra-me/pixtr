package me.rparra.pm_pixtr.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.fragments.PhotoViewerFragment;
import me.rparra.pm_pixtr.utils.StorageManager;

public class PhotoViewerActivity extends AppCompatActivity implements
        PhotoViewerFragment.OnFragmentInteractionListener {
    private final String TAG = PhotoViewerActivity.class.getName();

    // Constants
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;

    // Views
    protected @BindView(R.id.activity_photo_viewer_viewpager) ViewPager viewPager;
    protected @BindView(R.id.activity_photo_viewer_layout_controls) LinearLayout controlsView;
    protected @BindView(R.id.activity_photo_viewer_image_button_share) ImageButton share;
    protected @BindView(R.id.activity_photo_viewer_image_button_filter) ImageButton filter;
    protected @BindView(R.id.activity_photo_viewer_image_button_delete) ImageButton delete;

    // Flags
    private boolean isVisible;

    // Etc.
    private final Handler mHideHandler = new Handler();
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            viewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            controlsView.setVisibility(View.VISIBLE);
        }
    };
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    // Listeners
    private final View.OnTouchListener delayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    /**
     * OnClick listener for the view pager
     * @param view
     */
    @OnClick(R.id.activity_photo_viewer_viewpager)
    public void onClickViewPager(View view) {
        toggle();
    }

    /**
     * OnClick listener for the share button
     * @param view
     */
    @OnClick(R.id.activity_photo_viewer_image_button_share)
    public void onClickShareButton(View view) {
        List<File> files = StorageManager.loadAllPictures(this);
        File current = files.get(viewPager.getCurrentItem());
        Intent sharePicture = new Intent(Intent.ACTION_SEND);
        sharePicture.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sharePicture.putExtra(Intent.EXTRA_STREAM, Uri.parse(current.getPath()));
        sharePicture.setType("image/jpeg");
        startActivity(sharePicture);
    }

    /**
     * OnClick listener for the filter button
     * @param view
     */
    @OnClick(R.id.activity_photo_viewer_image_button_filter)
    public void onClickFilterButton(View view) {
        List<File> files = StorageManager.loadAllPictures(this);
        File current = files.get(viewPager.getCurrentItem());
        Intent photoEdit = new Intent(PhotoViewerActivity.this, PhotoEditorActivity.class);
        photoEdit.putExtra(PhotoEditorActivity.EXTRA_FILENAME, current.getName());
        startActivity(photoEdit);
    }

    /**
     * OnClick listener for the delete button
     * TODO: Actually delete the picture!
     * @param view
     */
    @OnClick(R.id.activity_photo_viewer_image_button_delete)
    public void onClickDeleteButton(View view) {
        Toast.makeText(PhotoViewerActivity.this, "Not implemented yet", Toast.LENGTH_SHORT).show();
    }

    private void setupViews() {
        Log.d(TAG, "setupViews: setting up views...");

        // Bind the views with ButterKnife
        ButterKnife.bind(this);

        isVisible = true;
        share.setOnTouchListener(delayHideTouchListener);
        filter.setOnTouchListener(delayHideTouchListener);
        delete.setOnTouchListener(delayHideTouchListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        setupViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PhotoPagerAdapter photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(photoPagerAdapter);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    private void toggle() {
        if (isVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        controlsView.setVisibility(View.GONE);
        isVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        viewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        isVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    protected void onDestroy() {
        System.gc();
        super.onDestroy();
    }

    @Override
    public void onFragmentInteraction(String s) {
        toggle();
    }

    /**
     * Custom FragmentStatePagerAdapter for the gallery
     */
    private class PhotoPagerAdapter extends FragmentStatePagerAdapter {
        private List<File> photoFiles;

        public PhotoPagerAdapter(FragmentManager fm) {
            super(fm);
            photoFiles = StorageManager.loadAllPictures(getApplicationContext());
        }

        @Override
        public Fragment getItem(int position) {
            return PhotoViewerFragment.newInstance(photoFiles.get(position).getName());
        }

        @Override
        public int getCount() {
            return photoFiles.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            System.gc();
            super.destroyItem(container, position, object);
        }
    }
}
