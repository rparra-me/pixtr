package me.rparra.pm_pixtr;

import android.app.Application;
import android.util.Log;

import me.rparra.pm_pixtr.utils.StorageManager;

public class PixtrApplication extends Application {
    private final String TAG = PixtrApplication.class.getName();

    // Global constants
    public static final String[] AVAILABLE_FILTERS = {
            "Normal",
            "Black and white",
            "Sepia",
            "Canny",
            "Binary",
            "Inverted"
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: creating app");

        // initialize storage
        StorageManager.initialize(this);
    }
}
