package me.rparra.pm_pixtr.adapters;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.rparra.pm_pixtr.PixtrApplication;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.utils.GeneralUtils;
import me.rparra.pm_pixtr.utils.OpenCvWrapper;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.ViewHolder> {
    private final String TAG = FiltersAdapter.class.getName();

    private OpenCvWrapper openCvWrapper;
    private Bitmap original;
    private OnItemClickListener listener;

    public FiltersAdapter(Bitmap original) {
        this.original = GeneralUtils.resizeBitmap(original, 640, 480);
        this.openCvWrapper = new OpenCvWrapper();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filter_preview, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.filterName.setText(PixtrApplication.AVAILABLE_FILTERS[position]);

        Bitmap preview = new OpenCvWrapper().filterFromPosition(position, original);
        if (preview != null) {
            holder.filterPreview.setImageBitmap(preview);
        }

        final int finalPosition = position;
        holder.containerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(finalPosition);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return PixtrApplication.AVAILABLE_FILTERS.length;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.adapter_filters_layout_container) LinearLayout containerLayout;
        @BindView(R.id.adapter_filters_text_view_name) TextView filterName;
        @BindView(R.id.adapter_filters_image_view_preview) CircleImageView filterPreview;

        public ViewHolder(View itemView) {
            super(itemView);

            // Bind views with ButterKnife
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int filterPosition);
    }
}
