package me.rparra.pm_pixtr.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;
import com.sromku.simple.storage.helpers.OrderType;

import java.io.File;
import java.util.List;

public class StorageManager {
    private static String TAG = StorageManager.class.getName();

    public static String GALLERY_NAME = "PixtrGallery";

    private static StorageManager ourInstance = new StorageManager();

    public static StorageManager getInstance() {
        return ourInstance;
    }

    private StorageManager() { /* */ }

    private static Storage getAvailableSotrage(Context context) {
        if (SimpleStorage.isExternalStorageWritable()) {
            return SimpleStorage.getExternalStorage();
        }
        else {
            return SimpleStorage.getInternalStorage(context);
        }
    }

    public static void initialize(Context context) {
        Log.d(TAG, "initialize: initializing storage");
        Storage storage = getAvailableSotrage(context);
        storage.createDirectory(GALLERY_NAME);
    }

    public static void savePicture(Context context, Bitmap picture, String timestamp) {
        Storage storage = getAvailableSotrage(context);
        storage.createFile(GALLERY_NAME, timestamp + ".jpg", picture);
    }

    public static void savePicture(Context context, byte[] picture, String timestamp) {
        Storage storage = getAvailableSotrage(context);
        storage.createFile(GALLERY_NAME, timestamp + ".jpg", picture);
    }

    public static List<File> loadAllPictures(Context context) {
        Storage storage = getAvailableSotrage(context);
        List<File> files = null;

        try {
            files = storage.getFiles(GALLERY_NAME, OrderType.DATE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return files;
    }

    public static Bitmap loadPicture(Context context, String name) {
        Storage storage = getAvailableSotrage(context);
        byte[] picture = storage.readFile(GALLERY_NAME, name);
        return BitmapFactory.decodeByteArray(picture, 0, picture.length);
    }

    public static Bitmap loadLastPicture(Context context) {
        Storage storage = getAvailableSotrage(context);
        Bitmap picture = null;
        try {
            List<File> files = storage.getFiles(GALLERY_NAME, OrderType.DATE);
            byte[] b = storage.readFile(GALLERY_NAME, files.get(0).getName());
            picture = BitmapFactory.decodeByteArray(b, 0, b.length);
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return picture;
    }

    public static String getFullDirectory(Context context) {
        Storage storage = getAvailableSotrage(context);
        List<File> files = storage.getFiles(GALLERY_NAME, OrderType.DATE);
        return files.get(0).getParent();
    }
}
