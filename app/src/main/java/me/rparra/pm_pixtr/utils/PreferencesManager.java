package me.rparra.pm_pixtr.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Preferences Manager
 */

public class PreferencesManager {
    private static final String TAG = PreferencesManager.class.getName();

    private static final String PREFS_NAME = "me.rparra.pm_pixtr.PREFERENCES";
    private static final String KEY_TUTORIAL = "me.rparra.pm_pixtr.KEY_TUTORIAL";
    private static final String KEY_SELFIE_CAMERA = "me.rparra.pm_pixtr.KEY_SELFIE_CAMERA";
    private static final String KEY_FLASH = "me.rparra.pm_pixtr_KEY_FLASH";

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    private static PreferencesManager ourInstance = new PreferencesManager();

    public static PreferencesManager getInstance() {
        return ourInstance;
    }

    private PreferencesManager() { /* */ }

    public static void setTutorialStatus(Context context, boolean finished) {
        Log.d(TAG, "setFinishedTutorial: setting tutorial to: " + finished);
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putBoolean(KEY_TUTORIAL, finished);
        editor.apply();
    }

    public static boolean getTutorialStatus(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean status = preferences.getBoolean(KEY_TUTORIAL, false);
        Log.d(TAG, "getTutorialStatus: tutorial status: " + status);
        return status;
    }

    public static void setSelfieCamera(Context context, boolean activated) {
        Log.d(TAG, "setSelfieCamera: setting selfie camera to: " + activated);
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putBoolean(KEY_SELFIE_CAMERA, activated);
        editor.apply();
    }

    public static boolean getSelfieCameraStatus(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean status = preferences.getBoolean(KEY_SELFIE_CAMERA, false);
        Log.d(TAG, "getSelfieCameraStatus: selfie camera status: " + status);
        return status;
    }

    public static void setFlash(Context context, boolean activated) {
        Log.d(TAG, "setFlash: setting flash to: " + activated);
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putBoolean(KEY_FLASH, activated);
        editor.apply();
    }

    public static boolean getFlashStatus(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean status = preferences.getBoolean(KEY_FLASH, false);
        Log.d(TAG, "getFlashStatus: status: " + status);
        return status;
    }
}
