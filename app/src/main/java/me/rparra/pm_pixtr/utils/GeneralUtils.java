package me.rparra.pm_pixtr.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.Random;

/**
 * Random utils
 */

public class GeneralUtils {
    private final String TAG = GeneralUtils.class.getName();

    /**
     * Returns the current bar height
     * @param context Android context
     * @return bar height
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Returns the current nav bar height
     * @param context Android context
     * @return nav bar height
     */
    public static int getNavigationBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Returns a funny slogan each time its called :)
     * Mostly taken from: https://www.shopify.com/tools/slogan-maker
     * @return slogan
     */
    public static String getRandomSlogan() {
        String[] slogans = {
                "Things go better with Pixtr",
                "The Pixtr that smiles back",
                "Pixtr, anytime of the day",
                "Say Pixtr!",
                "Did somebody say Pixtr?",
                "We're with the Pixtr",
                "Pixtr for everybody",
                "Pixtr - Your personal entertainer",
                "Pixtr makes you sexy!",
                "You've always got time for Pixtr",
                "The joy of Pixtr",
                "Get Pixtr, forget the rest",
                "Pixtr for all time"
        };

        return slogans[new Random().nextInt(slogans.length)];
    }

    public static String getUnixTimeStamp() {
        long unixTime = System.currentTimeMillis() / 1000L;
        return String.valueOf(unixTime);
    }

    public static Bitmap resizeBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public static int getRandomAccentColor() {
        String[] colors = { // TODO: Use resources!
                "#c8154c",
                "#f5a32a",
                "#79c1b5"
        };

        return Color.parseColor(colors[new Random().nextInt(colors.length)]);
    }
}
