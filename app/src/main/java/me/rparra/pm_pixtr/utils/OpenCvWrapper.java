package me.rparra.pm_pixtr.utils;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

/**
 * Created by ropg on 17/05/26.
 */

public class OpenCvWrapper {
    private final String TAG = OpenCvWrapper.class.getName();

    public OpenCvWrapper() {

    }

    public Bitmap filterFromPosition(int position, Bitmap original) {
        switch (position) {
            case 0: // Normal
                return original;
            case 1: // Black and white
                return filterBlackWhite(original);
            case 2: // Sepia
                return filterSepia(original);
            case 3: // Canny
                return filterCanny(original);
            case 4: // Binary
                return filterBinary(original);
            case 5: // Inverted
                return filterInverted(original);
            default:
                return original;
        }
    }

    public Bitmap filterBlackWhite(Bitmap inputImage) {
        Mat inputMat = new Mat();
        Mat outputMat = new Mat();
        Bitmap outputImage = Bitmap.createBitmap(inputImage);

        Utils.bitmapToMat(inputImage, inputMat);
        nativeFilterBlackWhite(inputMat.getNativeObjAddr(), outputMat.getNativeObjAddr());
        Utils.matToBitmap(outputMat, outputImage);

        return outputImage;
    }

    // TODO: Not sepia :(
    public Bitmap filterSepia(Bitmap inputImage) {
        Mat inputMat = new Mat();
        Mat outputMat = new Mat();
        Bitmap outputImage = Bitmap.createBitmap(inputImage);

        Utils.bitmapToMat(inputImage, inputMat);
        nativeFilterSepia(inputMat.getNativeObjAddr(), outputMat.getNativeObjAddr());
        Utils.matToBitmap(outputMat, outputImage);
        return outputImage;
    }

    public Bitmap filterCanny(Bitmap inputImage) {
        Mat inputMat = new Mat();
        Mat outputMat = new Mat();
        Bitmap outputImage = Bitmap.createBitmap(inputImage);

        Utils.bitmapToMat(inputImage, inputMat);
        nativeFilterCanny(inputMat.getNativeObjAddr(), outputMat.getNativeObjAddr());
        Utils.matToBitmap(outputMat, outputImage);
        return outputImage;
    }

    public Bitmap filterBinary(Bitmap inputImage) {
        Mat inputMat = new Mat();
        Mat outputMat = new Mat();
        Bitmap outputImage = Bitmap.createBitmap(inputImage);

        Utils.bitmapToMat(inputImage, inputMat);
        nativeFilterBinary(inputMat.getNativeObjAddr(), outputMat.getNativeObjAddr());
        Utils.matToBitmap(outputMat, outputImage);
        return outputImage;
    }

    public Bitmap filterInverted(Bitmap inputImage) {
        Mat inputMat = new Mat();
        Mat outputMat = new Mat();
        Bitmap outputImage = Bitmap.createBitmap(inputImage);

        Utils.bitmapToMat(inputImage, inputMat);
        nativeFilterInvert(inputMat.getNativeObjAddr(), outputMat.getNativeObjAddr());
        Utils.matToBitmap(outputMat, outputImage);
        return outputImage;
    }

    private native void nativeFilterBlackWhite(long nativeObjMat, long output);
    private native void nativeFilterSepia(long nativeObjMat, long nativeObjOutput);
    private native void nativeFilterCanny(long nativeObjMat, long nativeObjOutput);
    private native void nativeFilterBinary(long nativeObjMat, long nativeObjOutput);
    private native void nativeFilterInvert(long nativeObjMat, long nativeObjOutput);

}
