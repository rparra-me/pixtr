package me.rparra.pm_pixtr.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import net.bohush.geometricprogressview.GeometricProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.rparra.pm_pixtr.R;
import me.rparra.pm_pixtr.utils.StorageManager;
import me.rparra.pm_pixtr.utils.GeneralUtils;

public class PhotoViewerFragment extends Fragment {
    private static final String TAG = PhotoViewerFragment.class.getName();

    // Fragment arguments
    private static final String ARG_FILENAME = "filename";
    private String filename;

    // Views
    protected @BindView(R.id.fragment_photo_viewer_image_view_photo) SubsamplingScaleImageView imagePhotoView;
    protected @BindView(R.id.fragment_photo_viewer_geometric_progress_view) GeometricProgressView progressView;

    // Listeners
    private OnFragmentInteractionListener fragmentInteractionListener;

    /**
     * OnClick listener for photo view
     * @param view
     */
    @OnClick(R.id.fragment_photo_viewer_image_view_photo)
    public void onClickPhotoView(View view) {
        if (filename != null) {
            onImagePressed(filename);
        }
    }

    private void setupViews(View view) {
        Log.d(TAG, "setupViews: setting up views...");

        // Bind views with ButterKnife
        ButterKnife.bind(this, view);

        // Random color for progress view
        progressView.setColor(GeneralUtils.getRandomAccentColor());
    }

    public PhotoViewerFragment() {
        // Required empty public constructor
    }

    public static PhotoViewerFragment newInstance(String filename) {
        PhotoViewerFragment fragment = new PhotoViewerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILENAME, filename);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filename = getArguments().getString(ARG_FILENAME);
            Log.d(TAG, "onCreate: loading fragment with picture " + filename);
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: destroying fragment with " + filename);

        // Hacky way to free memory :(
        imagePhotoView.recycle();
        imagePhotoView = null;
        System.gc();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        setupViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AsyncLoadPhoto loadPhoto = new AsyncLoadPhoto();
        loadPhoto.execute(filename);
    }

    public void onImagePressed(String t) {
        if (fragmentInteractionListener != null) {
            fragmentInteractionListener.onFragmentInteraction(t);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            fragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String s);
    }

    /**
     * Asynchronously loads the current photo
     */
    private class AsyncLoadPhoto extends AsyncTask<String, String, String> {
        private String picturePath;

        @Override
        protected String doInBackground(String... params) {
            picturePath = StorageManager.getFullDirectory(getContext()) + "/" + filename;
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (imagePhotoView != null) {
                imagePhotoView.setImage(ImageSource.uri(picturePath));
            }
        }
    }
}
