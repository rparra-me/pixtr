package me.rparra.pm_pixtr.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.rparra.pm_pixtr.R;

public class TutorialFragment extends Fragment {
    private final String TAG = TutorialFragment.class.getName();

    // Fragment arguments
    private static final String ARG_FRAGMENT_NUMBER = "fragmentNumber";
    private int fragmentNumber;

    // Views
    protected @BindView(R.id.fragment_tutorial_image_view) ImageView image;
    protected @BindView(R.id.fragment_tutorial_text_view_title) TextView textTitle;
    protected @BindView(R.id.fragment_tutorial_text_view_description) TextView textDescription;

    // Resources
    protected @BindDrawable(R.drawable.tutorial_camera) Drawable drawableCamera;
    protected @BindDrawable(R.drawable.tutorial_filter) Drawable drawableFilter;
    protected @BindDrawable(R.drawable.tutorial_share) Drawable drawableShare;
    protected @BindString(R.string.take_a_picture) String takeAPicture;
    protected @BindString(R.string.take_a_picture_description) String takeAPictureDescription;
    protected @BindString(R.string.filter_it) String filterIt;
    protected @BindString(R.string.filter_it_description) String filterItDescription;
    protected @BindString(R.string.share_it) String shareIt;
    protected @BindString(R.string.share_it_description) String shareItDescription;

    /**
     * Setup views on this fragment
     * @param view
     */
    private void setupViews(View view) {
        Log.d(TAG, "setupViews: setting up the views...");

        // Setup the views with ButterKnife
        ButterKnife.bind(this, view);

        Drawable currentDrawable = null;
        String currentTitle, currentDescription;

        switch (fragmentNumber) {
            case 0:
                currentDrawable = drawableCamera;
                currentTitle = takeAPicture;
                currentDescription = takeAPictureDescription;
                break;
            case 1:
                currentDrawable = drawableFilter;
                currentTitle = filterIt;
                currentDescription = filterItDescription;
                break;
            case 2:
                currentDrawable = drawableShare;
                currentTitle = shareIt;
                currentDescription = shareItDescription;
                break;
            default:
                currentDrawable = drawableCamera;
                currentTitle = takeAPicture;
                currentDescription = takeAPictureDescription;
                break;
        }

        image.setImageDrawable(currentDrawable);
        textTitle.setText(currentTitle);
        textDescription.setText(currentDescription);
    }

    public TutorialFragment() {

    }

    public static TutorialFragment newInstance(int fragmentNumber) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_FRAGMENT_NUMBER, fragmentNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fragmentNumber = getArguments().getInt(ARG_FRAGMENT_NUMBER);
            Log.d(TAG, "onCreate: creating fragment number " + fragmentNumber);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        setupViews(view);
        return view;
    }

}
