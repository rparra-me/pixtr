#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

extern "C" {

JNIEXPORT void JNICALL
Java_me_rparra_pm_1pixtr_utils_OpenCvWrapper_nativeFilterInvert(JNIEnv *env, jobject instance,
                                                                jlong nativeObjMat,
                                                                jlong nativeObjOutput) {
    cv::Mat* inputImage = (cv::Mat*) nativeObjMat;
    cv::Mat* outputImage = (cv::Mat*) nativeObjOutput;
    cv::bitwise_not(*inputImage, *outputImage);
}

JNIEXPORT void JNICALL
Java_me_rparra_pm_1pixtr_utils_OpenCvWrapper_nativeFilterBinary(JNIEnv *env, jobject instance,
                                                                jlong nativeObjMat,
                                                                jlong nativeObjOutput) {
    cv::Mat gray, dst;
    cv::Mat* inputImage = (cv::Mat*) nativeObjMat;
    cv::Mat* outputImage = (cv::Mat*) nativeObjOutput;

    cv::cvtColor(*inputImage, gray, CV_RGB2GRAY);
    cv::threshold(gray, dst, 100, 255, 0);
    dst.copyTo(*outputImage);
}

JNIEXPORT void JNICALL
Java_me_rparra_pm_1pixtr_utils_OpenCvWrapper_nativeFilterCanny(JNIEnv *env, jobject instance,
                                                               jlong nativeObjMat,
                                                               jlong nativeObjOutput) {
    cv::Mat gray, edge, draw;
    cv::Mat* inputImage = (cv::Mat*) nativeObjMat;
    cv::Mat* outputImage = (cv::Mat*) nativeObjOutput;

    cv::cvtColor(*inputImage, gray, CV_RGB2GRAY);
    cv::Canny(gray, edge, 50, 150, 3);
    edge.convertTo(draw, CV_8U);
    draw.copyTo(*outputImage);
}

JNIEXPORT void JNICALL
Java_me_rparra_pm_1pixtr_utils_OpenCvWrapper_nativeFilterSepia(JNIEnv *env, jobject instance,
                                                               jlong nativeObjMat,
                                                               jlong nativeObjOutput) {
    cv::Mat* inputImage = (cv::Mat*) nativeObjMat;
    cv::Mat* outputImage = (cv::Mat*) nativeObjOutput;

    cv::Mat kern = (cv::Mat_<float>(4,4) <<  0.272, 0.534, 0.131, 0,
            0.349, 0.686, 0.168, 0,
            0.393, 0.769, 0.189, 0,
            0, 0, 0, 1);

    cv::transform(*inputImage, *outputImage, kern);
}

JNIEXPORT void JNICALL
Java_me_rparra_pm_1pixtr_utils_OpenCvWrapper_nativeFilterBlackWhite(JNIEnv *env, jobject instance,
                                                                    jlong nativeObjMat, jlong nativeObjOut) {

    cv::Mat* inputImage = (cv::Mat*) nativeObjMat;
    cv::Mat* outputImage = (cv::Mat*) nativeObjOut;

    cv::cvtColor( *inputImage, *outputImage, cv::COLOR_RGB2GRAY);
}
}

